-- Alexandre Kahn
-- 500067
-- alexandre.kahn@vub.be
import qualified System.Environment
import Data.List
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Foldable

type Coordinates = (Int, Int)

data WindDirections = North | East | South | West deriving (Eq, Enum, Bounded,Show)
next :: WindDirections -> WindDirections
next West = North
next x = succ x

data NeighbourQueue = NeighbourQueue (Set Coordinates) [PointNode] deriving (Show)
-- We keep all the PointNodes we visited in a Set. We can use this to efficiently check if we already visited this node 
-- The ones we still need to visit are put in a list

data PointNode = Leaf | PointNode (Int, Int) PointNode deriving (Show, Eq, Ord)
-- A PointNode is will keep all the previous PointNodes, this is done to efficiently backtrack


main :: IO ()
main = do [path] <- System.Environment.getArgs
          maze <- readFile path
          putStr $ unlines $ escape $ lines maze
          

escape :: [[Char]] -> [[Char]]
escape maze  = 
  backtrack maze start endPointNode
  where
    start = findStart maze
    end = findEnd maze
    queue = addNeighbours maze North (PointNode start Leaf) (NeighbourQueue Set.empty [])
    endPointNode = mazerunner maze (PointNode start Leaf) end queue
    -- The escape function will first find the starting and ending point of the maze
    -- Then it will add the direct neighbours of the starting point to the NeigbourQueue
    -- Then it will start searching the path
    -- Once the path is found it will backtrack to mark the path


noWall :: [[Char]] -> Coordinates -> Bool
noWall maze (x, y) = ((maze !! y) !! x) /= head "X"
-- This function will check if their is a wall at that location

printElem :: [[Char]] -> Coordinates -> IO()
printElem maze (x, y) = putChar $ ((maze !! y) !! x) 
-- Function used purely for debugging. Shows the maze at a coordinate

findStart :: [[Char]] -> Coordinates
findStart maze =
    outerFind maze "*" 0

findEnd :: [[Char]] -> Coordinates
findEnd maze =
  outerFind maze "@" 0

outerFind :: [[Char]] -> [Char] -> Int -> Coordinates
outerFind maze sign y 
  | maze == [] = (-1, -1)
  | x == -1 = outerFind (tail maze) sign (y + 1)
  | otherwise = (x,y)
  where
   x = innerFind(head maze) sign 0
-- Searches a certain character inside the maze. This functions goes over the y-axis, while the innerFind goes over the x-axis

innerFind :: [Char] -> [Char] -> Int -> Int
innerFind row sign x 
  | row == [] = -1
  | (head row) == (head sign) = x
  | otherwise = innerFind (tail row) sign (x + 1)

mazerunner :: [[Char]] -> PointNode -> Coordinates -> NeighbourQueue -> Maybe PointNode
mazerunner maze Leaf _ _ = Nothing
mazerunner maze (PointNode (currentX, currentY) prev) end (NeighbourQueue set queue)
  | current == end = Just prev -- Stop condition
  | queue == [] = Nothing -- Should only happen if no viable way
  | otherwise = mazerunner maze nextPointnode end neigbours
  where
    nextPointnode = head queue
    current = (currentX, currentY)
    remainingSet = Set.insert (currentX, currentY) set
    remainingQueue = tail queue
    neighbourQueue = NeighbourQueue remainingSet remainingQueue
    neigbours = addNeighbours maze North (PointNode current prev) neighbourQueue
    -- This function recursively visits a node and checks if we arrived at the end. If their are no neighbours left the maze is unsolvable. Otherwise it will go to the next neighbour.

addNeighbours :: [[Char]] -> WindDirections -> PointNode -> NeighbourQueue -> NeighbourQueue
addNeighbours maze windDirection pointnode queue 
  | windDirection == West = neighbourQueue -- return the pointnodes with the new West
  | otherwise = 
      addNeighbours maze nextDirection pointnode neighbourQueue -- call this function with the new WindDirection
  where
    nextDirection = next windDirection
    neighbourQueue = findNeighbour maze windDirection pointnode queue


findNeighbour :: [[Char]] -> WindDirections -> PointNode -> NeighbourQueue -> NeighbourQueue
findNeighbour maze windDirection (PointNode (x, y) Leaf) (NeighbourQueue set queue) 
  | noWall maze (xNew, yNew) = neighbourQueue
  | otherwise = NeighbourQueue set queue
  where 
    (xNew, yNew) = newCoordinates windDirection (x,y)
    neighbourQueue = NeighbourQueue set (queue ++ [(PointNode (xNew, yNew) (PointNode (x, y) Leaf))]) -- Lazy evaluation makes this possible
findNeighbour maze windDirection (PointNode (x, y) (PointNode (xPrev, yPrev) prev)) (NeighbourQueue set queue) 
  | safe && notPrevious && notVisited = neighbourQueue
  | otherwise = NeighbourQueue set queue
  where 
    (xNew, yNew) = newCoordinates windDirection (x,y)
    notPrevious = (xNew, yNew) /= (xPrev, yPrev)
    notVisited = Set.notMember (xNew, yNew) set
    safe = noWall maze (xNew, yNew)
    neighbourQueue = NeighbourQueue set (queue ++ [(PointNode (xNew, yNew) (PointNode (x, y) (PointNode (xPrev, yPrev) prev)))])
    -- Adds neighbours to the queue when they are available. 

newCoordinates :: WindDirections -> Coordinates -> Coordinates
newCoordinates North (x,y) = (x, y - 1)
newCoordinates South (x,y) = (x, y + 1)
newCoordinates East (x,y) = (x + 1, y)
newCoordinates West (x,y) = (x - 1, y)
-- Abstraction for the calculation of the coordinates of the neighbours

backtrack :: [[Char]] -> Coordinates -> Maybe PointNode -> [[Char]]
backtrack _ _ Nothing = ["No path is found!"]
backtrack maze _ (Just Leaf) = ["No path is found!"]
backtrack maze (startX, startY) (Just (PointNode (x, y) prev)) 
    | startX == x && startY == y = maze -- Stop condition
    | otherwise = backtrack breadcrumbMaze (startX, startY) (Just prev)
    where
      breadcrumbMaze = breadcrumb maze (x,y)
      -- Function that does the backtracking to correctly mark the path 
      
putDot :: Int -> [Char] -> [Char]
putDot idx list = x ++ "." ++ ys  
      where (x,_:ys) = splitAt idx list

replace :: Int -> [Char] -> [[Char]] -> [[Char]]
replace idx string list = x ++ string : ys  
      where (x,_:ys) = splitAt idx list

breadcrumb :: [[Char]] -> Coordinates -> [[Char]]
breadcrumb maze (x, y) =  replace y sublist maze
      where 
        sublist = putDot x  $ maze !! y
  